
import os

import unittest
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By


os.environ["ANDRIOD_HOME"] = "C:\\Users\\xavi\\AppData\\Local\\Android\\sdk"

desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '5.1'
desired_caps['deviceName'] = 'Lenovo A2010-a'
desired_caps['app'] = 'e:\\appium\\e.apk'

driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)

WebDriverWait(driver,60).until(expected_conditions.presence_of_element_located((By.XPATH,"//android.webkit.WebView[@content-desc=\"eCrimp - Select Crimper\"]/android.widget.ListView/android.view.View[1]")))

el = driver.find_element_by_id('select-crimper-0')
action = TouchAction(driver)
action.tap(el).perform()
back = driver.find_element_by_id('Crimpers')
action.tap(back).perform()
el = driver.find_element_by_id('select-crimper-1')
action.tap(el).perform()
action.tap(back).perform()

driver.quit()

