import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.openqa.selenium.By;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testobject.rest.api.appium.common.TestObjectCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testobject.appium.junit.*;
import java.net.URL;
import java.util.List;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


import static org.junit.Assert.*;
import com.google.common.collect.ImmutableSet;

public class apptest {

    /* This is the key piece of our test, since it allows us to
   * connect to the device we will be running the app onto.*/
    private AppiumDriver driver;
    //public static AppiumDriver<WebElement> driver;


    /* Sets the test name to the name of the test method. */
    @Rule
    public TestName testName = new TestName();

    /* Takes care of sending the result of the tests over to TestObject. */
    @Rule
    public TestObjectTestResultWatcher resultWatcher = new TestObjectTestResultWatcher();

    /* This is the setup that will be run before the test. */
    @Before
    public void setUp() throws Exception {

        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("testobject_api_key", "5D9CACC18BD14F77B9839514D929879D");
        capabilities.setCapability("testobject_device", "Motorola_Moto_E_2nd_gen_free");

        driver = new AndroidDriver(new URL("https://eu1.appium.testobject.com/wd/hub"), capabilities);

        /* IMPORTANT! We need to provide the Watcher with our initialized AppiumDriver */
        resultWatcher.setRemoteWebDriver(driver);

    }

    /* IMPORTANT! driver.quit() is not called anymore, as the Watcher is now
       taking care of this. You can get rid of the tearDown method. */

    @Test
    public void testMethod() {
        /* Your test. */
        WebElement crimperSelect = (new WebDriverWait(driver,60)).
                until(ExpectedConditions.presenceOfElementLocated(By.id("select-crimper-7")));
        crimperSelect.click();
        // check whether we are on the next page
        WebElement crimperName = driver.findElement(By.id("crimper-name"));
        assertEquals(crimperName.getAttribute("name"),"PC707");

        // move back to home screen
        WebElement backButton = driver.findElement(By.id("Crimpers"));
        backButton.click();
        //error checking
        WebElement mainMenuLabel = driver.findElement(By.id("select-crimper-label"));
        assertEquals(mainMenuLabel.getAttribute("name"),"Select a crimper (1.2.14)");

        crimperSelect.click();

        // select hose button
        WebElement industrial = driver.findElement(By.id("select-industrial-hose"));
        WebElement hydraulic = driver.findElement(By.id("select-hydraulic-hose"));
        industrial.click();

        WebElement industrialValue1 = driver.findElement(By.id("select-hose-0"));
        assertEquals(industrialValue1.getAttribute("name"),"1000MP/Mine Spray");
        //select-hose-type-container
        hydraulic.click();

        // select value from hose
        WebElement hoseBlock = driver.findElement(By.xpath("//android.webkit.WebView[@content-desc=\"eCrimp - Select Options\"]/android.view.View[2]/android.view.View[1]"));
        hoseBlock.click();
        WebElement hoseValue = driver.findElement(By.id("select-hose-123"));
        hoseValue.click();
        assertEquals(driver.findElement(By.id("select-label-hose")).getAttribute("name"),"C5C");

        // select value from dash
        WebElement dashBlock = driver.findElement(By.xpath("//android.webkit.WebView[@content-desc=\"eCrimp - Select Options\"]/android.view.View[2]/android.view.View[2]"));
        dashBlock.click();
        WebElement dashValue = driver.findElement(By.id("select-dash-5"));
        dashValue.click();
        assertEquals(driver.findElement(By.id("select-label-dash")).getAttribute("name"),"8");

        WebElement stemBlock = driver.findElement(By.xpath("//android.webkit.WebView[@content-desc=\"eCrimp - Select Options\"]/android.view.View[2]/android.view.View[3]"));
        stemBlock.click();
        WebElement stemValue = driver.findElement(By.id("select-stem-6"));
        stemValue.click();
        assertEquals(driver.findElement(By.id("select-label-stem")).getAttribute("name"),"G");

        WebElement resetButton = driver.findElement(By.id("reset-button"));
        resetButton.click();

        hoseBlock.click();
        hoseValue.click();
        dashBlock.click();
        dashValue.click();
        stemBlock.click();
        stemValue.click();


        WebElement results = driver.findElement(By.xpath("//android.webkit.WebView[@content-desc=\"eCrimp - Select Options\"]/android.view.View[4]\n"));
        results.click();

        // result screen
        assertEquals(driver.findElement(By.id("set-die")).getAttribute("name"),"732");

        //List<WebElement> x =  driver.findElements(By.className("android.widget.Button"));
        //x.get(0).click();
        //WebElement emailButton = (new WebDriverWait(driver,60)).
          //      until(ExpectedConditions.presenceOfElementLocated(By.id("email-button")));
        //email-button
        //select-metric
        //System.out.println(driver.getContext());

        WebElement email = driver.findElement(By.id("email-button"));
        email.click();
        assertEquals("vx","vx");
        //crimper-name
        //reset-button
        //[Hose buttom
        //select-option-hose-arrow
        // select-label-hose
        //]
        /*
        [
        //android.webkit.WebView[@content-desc="eCrimp - Select Options"]/android.view.View[2]/android.view.View[2]
        Dash Size
        select-label-dash
        select-option-dash-arrow
        ]

        ]
        [
          //android.webkit.WebView[@content-desc="eCrimp - Select Options"]/android.view.View[2]/android.view.View[3]
        Stem
        select-label-stem
        select-option-stem-arrow
        ]
        view-results-button
        select-hose-123
        select-dash-1
        select-stem-0
        */

    }

}